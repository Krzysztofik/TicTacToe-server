﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Net.Sockets;
using System.Net;
using System.Threading;
using Newtonsoft.Json.Linq;
namespace Tic_Tac_Toe
{
    public partial class Form1 : Form
    {
        private int height;
        private int width;
        int ButtonWidth = 40;
        int ButtonHeight = 40;
        int Distance = 20;
        int start_x = 10;
        int start_y = 10;
        private static readonly int Start_Value = 3;
        private int value = 3;
        private TcpListener server;
        private string[,] array;
        TcpClient mCilent1;
        TcpClient mCilent2;
        Socket listener;
        public Form1()
        {
            InitializeComponent();
            creatPlaceGame(Start_Value);
            listener = configuration();
        }

        private void creatPlaceGame(int value)
        {
            array = new string[value, value];
            FlowLayoutPanel mainPanel = new FlowLayoutPanel();
            mainPanel.Name = "main";
            for (int x = 0; x < value; x++)
            {
                FlowLayoutPanel panel = new FlowLayoutPanel();
                panel.Name = "Panel";
                for (int y = 0; y < value; y++)
                {
                    Button tmpButton = new Button();
                    tmpButton.Left = start_y + (y * ButtonWidth + Distance);
                    tmpButton.Width = ButtonWidth;
                    tmpButton.Height = ButtonHeight;
                    tmpButton.Name = y + " " + x;
                    panel.Controls.Add(tmpButton);
                    array[x, y] = "";

                }
                panel.Top = start_x + (x * ButtonHeight + Distance);
                panel.Left = 0;
                panel.Height = ButtonHeight + start_y;
                panel.Width = value * (ButtonWidth + Distance) + start_x;
                mainPanel.Controls.Add(panel);
            }
            mainPanel.Width = value * (ButtonWidth + Distance) + start_x;
            mainPanel.Height = value * (ButtonHeight + Distance);
            this.Controls.Add(mainPanel);
            layoutPanel.Left = mainPanel.Width + 50;
            
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Controls.RemoveAt(this.Controls.Count - 1);
            try
            {
                value = Int16.Parse(value_x.Text);
                if (value < 3)
                    value = 3;

            }
            catch (FormatException x)
            {
                value_x.Text = "value must be number";

            }

            creatPlaceGame(value);
        }

        private void updateTable(int x,int y,string text)
        {
            var a = this.Controls[1];
            var b = a.Controls[y];
            var c = b.Controls[x];
            if (text.Equals("X"))
                 c.BackColor = Color.Red;
            else
                c.BackColor = Color.Blue;
            c.Text=text;
            this.Update();
            /*this.Controls.RemoveAt(this.Controls.Count - 1);
            FlowLayoutPanel mainPanel = new FlowLayoutPanel();
            for (int x = 0; x < value; x++)
            {
                FlowLayoutPanel panel = new FlowLayoutPanel();
                for (int y = 0; y < value; y++)
                {
                    Button tmpButton = new Button();
                    tmpButton.Left = start_y + (y * ButtonWidth + Distance);
                    tmpButton.Width = ButtonWidth;
                    tmpButton.Height = ButtonHeight;
                    tmpButton.Text = array[x, y];
                    panel.Controls.Add(tmpButton);


                }
                panel.Top = start_x + (x * ButtonHeight + Distance);
                panel.Left = 0;
                panel.Height = ButtonHeight + start_y;
                panel.Width = value * (ButtonWidth + Distance) + start_x;
                mainPanel.Controls.Add(panel);
            }
            mainPanel.Width = value * (ButtonWidth + Distance) + start_x;
            mainPanel.Height = value * (ButtonHeight + Distance);
            this.Controls.Add(mainPanel);
            layoutPanel.Left = mainPanel.Width + 50;*/
        }

        private Socket configuration()
        {
            IPHostEntry ipHostInfo = Dns.GetHostEntry(Dns.GetHostName());
            IPAddress ipAddress = ipHostInfo.AddressList[0];
            IPEndPoint localEndPoint = new IPEndPoint(ipAddress, 11000);
            Socket listener = new Socket(ipAddress.AddressFamily,
                SocketType.Stream, ProtocolType.Tcp);
            listener.Bind(localEndPoint);
            listener.Listen(10);
            return listener;
        }

        private string readMessage(Socket handler)
        {
            string data = null;
            byte[] bytes = new Byte[1024];
            data = null;
            while (true)
            {
                bytes = new byte[1024];
                int bytesRec = handler.Receive(bytes);
                data += Encoding.ASCII.GetString(bytes, 0, bytesRec);
                if (data.IndexOf("<EOF>") > -1)
                {
                    break;
                }
            }
            data = data.Replace("<EOF>", "");
            Console.WriteLine("Text received : {0}", data);
            return data;
        }
        private void sendMessage(Socket handler, string message)
        {
            byte[] msg = Encoding.ASCII.GetBytes(message);
            handler.Send(msg);
        }
        private void button2_Click(object sender, EventArgs e)
        {
            int senderMessage = 1;
            int reader = 0;  
            List<Socket> handlers = new List<Socket>();
            try
            {
                Console.WriteLine("Waiting for a connection...");
                while (handlers.Count < 2)
                {
                    handlers.Add(listener.Accept());
                    Console.WriteLine("Player {0} connection", handlers.Count);
                }
                //first send
                var json = Newtonsoft.Json.JsonConvert.SerializeObject(new { player = 0, size = value });
                sendMessage(handlers[reader], json.ToString());

                //first call

                json = Newtonsoft.Json.JsonConvert.SerializeObject(new { player = 1, size = value });
                int counter = 0;
                sendMessage(handlers[senderMessage], json.ToString());
                while (true)
                {
                    String response = readMessage(handlers[reader]);
                    dynamic v = JObject.Parse(response);
                    int x_val = v.x;
                    int y_val = v.y;
                    if (reader == 0)
                    {
                        array[x_val, y_val] = "X";
                        updateTable(x_val, y_val,"X");
                    }
                    else
                    {
                        array[x_val, y_val] = "Y";
                        updateTable(x_val, y_val, "Y");
                    }
                    //updateTable();
                    json = Newtonsoft.Json.JsonConvert.SerializeObject(new { x = x_val, y = y_val });
                    sendMessage(handlers[senderMessage], json.ToString());
                    int temp = reader;
                    reader = senderMessage;
                    senderMessage = temp;
                    checkBoard(handlers, senderMessage,reader );
                    counter += 1;
                    if (counter == value * value)
                    {
                        sendResult(handlers, senderMessage, reader, "draw");

                    }
                    if (!handlers[reader].Connected || !handlers[senderMessage].Connected)
                    {
                        break;
                    }
                }
                //handler.Shutdown(SocketShutdown.Both);
                // handler.Close();


            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }

            Console.WriteLine("\nPress ENTER to continue...");
            Console.Read();

        }
        void checkBoardMulti(List<Socket> handlers, int sender, int reader,string[,]array,int value) {
            bool flag = true;
            
            for (int j = 0; j < value; j++)
            {
                string lastValue = array[j, 0];
                if (!lastValue.Equals(""))
                {
                    for (int i = 1; i < value; i++)
                    {
                        if (lastValue != array[j, i])
                        {
                            flag = false;
                            break;
                        }
                    }
                    if (flag)
                    {
                        sendResult(handlers, sender, reader, "");
                        break;
                    }
                    flag = true;
                }
            }

            for (int j = 0; j < value; j++)
            {
                string lastValue = array[0, j];
                if (!lastValue.Equals(""))
                {
                    for (int i = 1; i < value; i++)
                    {
                        if (lastValue != array[i, j])
                        {
                            flag = false;
                            break;
                        }
                    }
                    if (flag)
                    {
                        sendResult(handlers, sender, reader, "");
                        break;
                    }
                    flag = true;
                }
            }
            if (!array[0, 0].Equals(""))
            {
                for (int i = 1; i < value; i++)
                {

                    if (array[0, 0] != array[i, i])
                    {
                        flag = false;
                        break;
                    }
                }
                if (flag)
                {
                    sendResult(handlers, sender, reader, "");
                }
            }

            flag = true;
            int temp = value - 1;
            if (!array[0, temp].Equals(""))
            {
                for (int j = 0; j < value; j++)
                {

                    if (array[j, temp - j] != array[0, temp])
                    {
                        flag = false;
                        break;
                    }
                }
                temp--;
                if (flag)
                {
                    sendResult(handlers, sender, reader, "");
                }
            }
        }

        void checkBoard(List<Socket> handlers, int sender, int reader)
        {
            

             if (value == 4 || value == 5 || value == 3)
             {
                checkBoardMulti(handlers, sender, reader, array,value);
             }
            else
            {
           int staticValue = 5;
                string[,] arrayCopy = new string[5, 5];
                for (int i = 0; i < value - staticValue; i++)
                {
                    for (int j = 0; j <value-staticValue; j++)
                    {
                        for(int k=0;k<staticValue; k++)
                        {
                            for (int g = 0; g < staticValue; g++)
                            {
                                arrayCopy[k, g] = array[k + i, g + j];
                            }
                          
                        }
                        checkBoardMulti(handlers, sender, reader, arrayCopy,staticValue);

                    }
                }

              /* for (int i = 0; i <= value-staticValue; i++)
               {
                   int co = 0;
                   for (int j = 0; j < value; j++)
                   {
                       flag = true;
                       if (!array[i, j].Equals(""))
                       {
                           for (int x = 0; x < staticValue; x++)
                           {
                               int c = i + x;
                               if (!array[i, j].Equals(array[c, j]))
                               {
                                   flag = false;
                                   break;
                               }
                           }
                           if (flag)
                           {
                               sendResult(handlers, sender, reader,"");
                               break;
                           }
                           flag = true;
                           for (int x = 0; x < staticValue; x++)
                           {
                               int c = i + x;
                               if (!array[i, j].Equals(array[j, c]))
                               {
                                   flag = false;
                                   break;
                               }
                           }
                           if (flag)
                           {
                               sendResult(handlers, sender, reader,"");
                               break;
                           }

                           if (j + staticValue <= value)
                           {
                           flag = true;
                           for (int x = 0; x < staticValue; x++)
                               {
                                   int c = i + x - co;
                                   if (!array[i, j].Equals(array[i + x, j + x]))
                                   {
                                       flag = false;
                                       break;
                                   }
                               }
                               Console.WriteLine();
                               if (flag)
                               {
                                   sendResult(handlers, sender, reader,"");
                                   break;
                               }

                           flag = true;
                           for (int x = 0; x < staticValue; x++)
                               {
                                   int c = i + x - co;

                                   if (!array[i+x, value-j-1].Equals(array[i + x, value-j-x-1]) || !array[i + x, value - j - x - 1].Equals(""))
                                   {
                                       flag = false;
                                       break;
                                   }
                               }

                               if (flag)
                               {
                                   sendResult(handlers, sender, reader,"");
                                   break;
                               }


                           }


                       }
                       flag = false;

                   }
                   co++;
                   if (flag)
                   {

                       break;
                   }
               }
*/
           }
        }
        private void sendResult(List<Socket> handlers, int sender, int reader,string draw)
        {
            if (draw.Equals("draw")) {
                var json = Newtonsoft.Json.JsonConvert.SerializeObject(new { status = "draw" });
                sendMessage(handlers[sender], json.ToString());
                json = Newtonsoft.Json.JsonConvert.SerializeObject(new { status = "draw" });
                sendMessage(handlers[reader], json.ToString());
            }
        
            else {
                var json = Newtonsoft.Json.JsonConvert.SerializeObject(new { status = "win" });
                sendMessage(handlers[sender], json.ToString());
                json = Newtonsoft.Json.JsonConvert.SerializeObject(new { status = "lose" });
                sendMessage(handlers[reader], json.ToString());
            }
           
            handlers[sender].Shutdown(SocketShutdown.Both);
            handlers[sender].Close();
            handlers[reader].Shutdown(SocketShutdown.Both);
            handlers[reader].Close();
        }
    }


}
